'use strict';

var gulp        = require('gulp'),
    // connectPHP  = require('node-php-server'),
    sass        = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    concat      = require('gulp-concat'),
    uglify      = require('gulp-uglify');

//script paths
var jsFiles     = './src/js',
    jsDest      = './app/static/js',
    cssFiles    = './app/static/css',
    sassFiles   = './src/sass',
    themeFiles  = './app/theme/granostudio-child',
    themeOrFiles= './src/theme';


// browserSync
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./app"
        }
    });
});

//compass
gulp.task('sass', function() {
  return gulp.src(sassFiles+"/**/**.scss")
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest(cssFiles));
});


// compact os js
gulp.task('scripts', function() {

    gulp.src(jsFiles+'/app.js')
        .pipe(concat('app.min.js'))
        .on('error', function (err) { console.log('[Error]', err.toString()); })
        .pipe(gulp.dest(jsDest));

    // config, controllers, services and components
    gulp.src([jsFiles+'/config/**.js', jsFiles+'/controller/**.js', jsFiles+'/services/**.js', jsFiles+'/components/**.js', jsFiles+'/components/**/**.js'])
        .pipe(concat('scripts.min.js'))
        .on('error', function (err) { console.log('[Error]', err.toString()); })
        .pipe(gulp.dest(jsDest));

    gulp.src(jsFiles+'/angular/0angular.min.js')
    .pipe(gulp.dest(jsDest+'/angular'));

    gulp.src([jsFiles+'/angular/modules/**.js', '!'])
    .pipe(concat('angular.min.js'))
    .on('error', function (err) { console.log('[Error]', err.toString()); })
    .pipe(gulp.dest(jsDest+'/angular'));

});

// watch para alterações
gulp.task('watch', function(){
  gulp.watch(['app/**/**.html', sassFiles+'/**.scss', sassFiles+'/**/**.scss', jsFiles+'/**/**', jsFiles+'/**'], ['sass', 'scripts'])
  .on('change', browserSync.reload);
});


// tasks grano
// grano-local para desv local
gulp.task('grano-local', ['browser-sync','scripts','sass','watch']);
