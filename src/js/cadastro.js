'use strict';
angular.
  module('iBikeSite')

  .component('ibCadastrado', {
    templateUrl:'/static/partial/cadastro/jacadastrado.html'


  }).component('ibCadastrar', {
    templateUrl:'/static/partial/cadastro/cadastrar.html'

  }).controller('cepCtrl', function($scope, myResource) {
           $scope.cep = "", $scope.city = null;
           $scope.findCep = function () {
             myResource.get({'cep': $scope.cep}).$promise
             .then(function success(result){
               $scope.city = result;
             }).catch(function error(msg) {
               console.error('Error');
             });
            }
      })
      .factory('myResource', function ($resource) {
        var rest = $resource(
            'https://viacep.com.br/ws/:cep/json/',
            {
              'cep': ''
            }
          );
          return rest;
      });
