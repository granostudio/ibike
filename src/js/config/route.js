var preachApp = angular.module('preachApp', ['ngRoute']);

preachApp.config(function($locationProvider) {
    $locationProvider.html5Mode(true);
});

angular.module('iBikeSite')

.config(['$routeProvider',
function config( $routeProvider){
  // $locationProvider.hashPrefix('!');

  $routeProvider.
      when('/meuspedidos', {
       templateUrl: 'meuspedidos.html'
      })
      .when('/alterar-senha', {
       templateUrl: 'alterar-senha.html'
      })
      .when('/cadastro-meus-dados', {
       templateUrl: 'cadastro-meus-dados.html'
      })
      .when('/efetuar-login2', {
       templateUrl: 'efetuar-login2.html'
      })
      .when('/cadastro', {
       templateUrl: 'cadastro.html'
      })
      .when('/produto-interna', {
       templateUrl: 'produto-interna.html'
      })
      .when('/acessorio-interna', {
       templateUrl: 'acessorio-interna.html'
      })
      // .when('/catalogo-produtos', {
      //  templateUrl: 'catalogo-produtos.html'
      // })
      .when('/catalogo-produtos-v2', {
       templateUrl: 'catalogo-produtos-v2.html'
      })
      .when('/catalogo-acessorios', {
        templateUrl: 'catalogo-acessorios.html'
      })
      .when('/confirmacao-compra', {
       templateUrl: 'confirmacao-compra.html'
      })
      .when('/bike-certa', {
       templateUrl: 'bike-certa.html'
      })
      .when('/experimentar-p2', {
       templateUrl: 'experimentar-p2.html'
      })
      .when('/experimentar-p3', {
       templateUrl: 'experimentar-p3.html'
      })
      .when('/experimentar', {
        templateUrl: 'experimentar-p3-resumo-pedido.html'
      })
      .when('/experimentar-p3-resumo-pedido', {
       templateUrl: 'experimentar-p3-resumo-pedido.html'
      })
      .when('/experimentar-p4', {
       templateUrl: 'experimentar-p4.html'
      })
      .when('/experimentar', {
       templateUrl: 'experimentar.html'
      })
      .when('/recuperar-senha', {
       templateUrl: 'recuperar-senha.html'
       })
      .when('/resumo-do-pedido', {
       templateUrl: 'resumo-do-pedido.html'
       })
       .when('/carrinho-de-compra-v3', {
        templateUrl: 'carrinho-de-compra-v3.html'
        })
      .when('/avaliacao', {
        templateUrl: 'avaliacao.html'
      })
      .when('/acompanhamento-pedido', {
        templateUrl: 'acompanhamento-pedido.html'
      })
      .when('/home', {
        templateUrl: 'home.html'
      })
      .otherwise({
          redirectTo: "/"
      });
}]);
