'use strict';
angular.module('iBikeSite')
.config([ '$mdThemingProvider', '$mdIconProvider',
    function config( $mdThemingProvider, $mdIconProvider) {

    // Tema e cores
    $mdThemingProvider.definePalette('iBikeColors', {
      '50': 'ffebee',
      '100': 'ffcdd2',
      '200': 'ef9a9a',
      '300': 'e57373',
      '400': 'ef5350',
      '500': 'fd7e14',
      '600': 'e53935',
      '700': 'd32f2f',
      '800': 'c62828',
      '900': 'b71c1c',
      'A100': 'fd7e14',
      'A200': 'fd7e14',
      'A400': 'fd7e14',
      'A700': 'fd7e14',
      'contrastDefaultColor': 'dark',    // whether, by default, text (contrast)
                                          // on this palette should be dark or light

      // 'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
      //  '200', '300', '400', 'A100'],
      'contrastLightColors': ['50', '100', //hues which contrast should be 'dark' by default
       '200', '300', '400', 'A100']    // could also specify this if default was 'dark'
    });

    $mdThemingProvider.theme('default')
      .primaryPalette('iBikeColors',{
        'default': '500', // by default use shade 400 from the pink palette for primary intentions
        'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
        'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
        'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
      })
      .accentPalette('iBikeColors');



}]);
