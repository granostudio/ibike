angular.module('iBikeSite')
.component('ibNavBusca', {
      templateUrl: 'components/navMenu/nav-busca.html',
      controller: function ibNavBuscaCrtl($element, $animate) {

        var group = angular.element(document.querySelector('#navBuscaGroup')),
            group2 = angular.element(document.querySelector('#busca2')).children().children(),
            input = $element.find('input');
        this.clicked = function(){
          if(!group.hasClass('active')){
            $animate.addClass(group, 'active');
            $animate.addClass(group2, 'active');
            input.focus();
          }
        }

        this.focused = function() {
          if(!group.hasClass('active')){
            $animate.addClass(group, 'active');
            $animate.addClass(group2, 'active');
          }
        }
         this.blurred = function() {
           if(group.hasClass('active')){
             $animate.removeClass(group, 'active');
             $animate.removeClass(group2, 'active');
           }
        }
      }
  });
