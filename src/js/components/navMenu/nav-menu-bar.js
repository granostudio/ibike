iBikeSite
  .component('navMenuBar', {
     bindings: {
       cliente: "<",
       menu: "<",
       preloader: "="
     },

     templateUrl:'components/navMenu/nav-menu-bar.html',
    controller: function navMenuCtrl($element, $location){
      var ctrl = this;
      ctrl.url = "";
      ctrl.posL = 0;
      ctrl.widhtS = 0;
      ctrl.style ={
        left: ctrl.posL+'px',
        width: ctrl.widhtS+'px',
      };
      ctrl.cliente = '';

      // ativar menuColpa
    ctrl.ativoMenuColp = true;
     ctrl.ativarMenuColp = function(){
       if(ctrl.ativoMenuColp){
         ctrl.ativoMenuColp = false;
       } else{
         ctrl.ativoMenuColp = true;
       }
     }
      ctrl.updatenline = function(w,p) {

      };

      this.mouseOver = function(e,  $event){
        var widht = e.currentTarget.clientWidth;
        var pos = e.currentTarget.offsetLeft;
          ctrl.style = {
            left: pos+'px',
            width: widht+'px',
          }
      }

      this.alterarPg = function(path){

      //  window.location.href =  path ;

       $location.path( path );
      }

    }
  });
