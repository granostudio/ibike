iBikeSite
  .controller('mainCtrl',function mainCtrl( $rootScope, $scope, $timeout ) {
    // preloader
    $scope.preloader=false;
    // angular.element(document).ready(function () {
    //     $timeout(desativarPreloader, 3000);
    // });
    // var desativarPreloader = function(){
    //   $scope.preloader=true
    // }

    var vm = this;

    $rootScope.navMenu = [
        {
           "titulo":"Quero experimentar",
           "url":"/meuspedidos",
        },
        {
           "titulo":"Quero comprar",
           "url":"/alterar-senha",
        },
        {
           "titulo":"Bike certa",
           "url":"/cadastro-meus-dados",
        },
        {
           "titulo":"Nossos acessórios",
           "url":"/cadastro",
        },
        {
           "titulo":"Fale conosco",
           "url":"/produto-interna",
        }
    ];
    $rootScope.cliente = {
          "firstname": "Bruce",
          "lastname": "Wayne",
          "nickname": "batman"
    };
    $rootScope.infos = {
      telefone: '0300 3132 420',
      whatsapp: '(11) 95065-9756',
      email: 'ibke@tembici.com.br',
      site: '',
      redessociais: {
        twitter: {
            url: '',
            icon: 'icon-twitter'
        },
        facebbok: {
            url: '',
            icon: 'icon-facebook'
          },
        instagram: {
          url: '',
          icon: 'icon-instagram'
        },
        youtube: {
          url: '',
          icon: 'icon-youtube'
        }
      }
    };
    $rootScope.paginas = {
        quemsomos: [
          {
            titulo: 'sobre o ibike',
            url: '/sobreoibike'
          },
          {
            titulo: 'trabalhe conosco',
            url: '/sobreoibike'
          },
          {
            titulo: 'política de privacidade',
            url: '/sobreoibike'
          },
          {
            titulo: 'política cookies',
            url: '/sobreoibike'
          },
          {
            titulo: 'termo de uso',
            url: '/sobreoibike'
          }    ,
          {
            titulo: 'a tembici',
            url: '/sobreoibike'
          }
        ],
        bicicletas: [
          {
            titulo: 'experimente',
            url: '/sobreoibike'
          },
          {
            titulo: 'compre com 50% de desconto*',
            url: '/sobreoibike'
          },
          {
            titulo: 'nossos acessórios',
            url: '/sobreoibike'
          },
          {
            titulo: 'descubra a bike certa',
            url: '/sobreoibike'
          },
          {
            titulo: 'dicas',
            url: '/sobreoibike'
          }    ,
          {
            titulo: 'a tembici',
            url: '/sobreoibike'
          }
        ],
        minhaconta: [
          {
            titulo: 'login',
            url: '/sobreoibike'
          },
          {
            titulo: 'ccadastre-se',
            url: '/sobreoibike'
          }
        ],
      };


    // get paginas from rootScope
    vm.paginas =  $rootScope.paginas;
    vm.infos =  $rootScope.infos;
    vm.cliente =  $rootScope.cliente;
    vm.navMenu =  $rootScope.navMenu;

  // console.log(vm);
    // preloader Route
    vm.progress = true;

    $rootScope.$on('$routeChangeStart', function(){
      vm.progress = true;
    });
    $rootScope.$on('$routeChangeSuccess', function(){
      $timeout(function() {
                  vm.progress = false;
            }, 1500);
    });

  });
