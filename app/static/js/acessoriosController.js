var urlPost = 'https://casainteligente.online/grano/listagemAcessorios.php';
iBikeSite.controller('acessoriosController', ['$scope', '$http','orderByFilter', function($scope,$http, orderBy) {
    $scope.buscarAcessorios = function(){
        var promise =
        $http({
            method  : 'GET',
            url     : urlPost
        });
        promise.then( function(result) {
            $scope.acessorios = result.data;
            console.log($scope.acessorios);
            $scope.campo = 'valor';
            $scope.reverse = true;
            $scope.acessorios = orderBy($scope.acessorios, $scope.campo , $scope.reverse);
            $scope.preloader = true;
            console.log($scope.preloader);
        }).catch ( function(result) {
          $scope.preloader = true;
          $scope.erro = true;
        });
    }

    $scope.buscarAcessorios();
    $scope.sortBy = function(campo,reverse){
        // $scope.reverse = true;
        // $scope.campo = campo ;
        // $scope.acessorios = orderBy($scope.acessorios, $scope.campo , $scope.reverse );
        $scope.ordenacao = campo;
        $scope.reverse = reverse;
    };
    $scope.valorFiltro = '';
    $scope.searchFilters = {};
    $scope.changeFilterTo = function(filtro,valor) {
      $scope.produtoFiltro = filtro;
      $scope.valorFiltro = valor;
      if(filtro == undefined){
          $scope.searchFilters = {};
      }else{
          if( $scope.searchFilters[filtro]){
                  delete $scope.searchFilters[filtro]
          }else{


      $scope.searchFilters[filtro] = valor;
      }
      }
      keys = (Object.keys($scope.searchFilters));
       $scope.strFiltros = '';
      for(i=0;i!=keys.length;i++){
          $scope.strFiltros = $scope.strFiltros+ keys[i]+ ' > ';

      }
      console.log($scope.searchFilters)


      }


    $scope.sidebarAtivado = true;
    $scope.ordenarAtivado = true;
  // animar botão filtrar
  $scope.ativarSidebar = function (){
    if(!$scope.sidebarAtivado ){
        $scope.sidebarAtivado = true;
    } else{
        $scope.sidebarAtivado = false;
    }
}
$scope.ativarOrdenar = function (){
    if(!$scope.ordenarAtivado ){
        $scope.ordenarAtivado = true;
    } else{
        $scope.ordenarAtivado = false;
    }
}


}]).factory('acessorios', function($http) {
  return {
    buscarAcessorios : function(){

        return (
            $http({
                method  : 'GET',
                url     : urlPost
            })
        )
    }
}
})
