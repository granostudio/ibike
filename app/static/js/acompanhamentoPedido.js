'use strict';


iBikeSite.controller('acompanhamentoPedido', ['$scope',  function($scope) {

  $scope.itens = [
    {
      inicioData: "10/05/2018",
      fimData: "10/06/2018"
    }
]

  $scope.dayDiff = function(inicioData,fimData){
        var date2 = new Date($scope.formatString(fimData));
        var date1 = new Date($scope.formatString(inicioData));
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
   }

   $scope.dayDiff = {

   }


    $scope.formatString = function(format) {
      var day   = parseInt(format.substring(0,2));
      var month  = parseInt(format.substring(3,5));
      var year   = parseInt(format.substring(6,10));
      var date = new Date(year, month-1, day);
      return date;
    }
}]);
