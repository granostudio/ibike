'use strict';
iBikeSite.controller('CarrinhoComprasController', ['$scope', '$http','helpers', function($scope,$http, helpers) {
	$scope.tabelaDescontos = {
		"descontoItau" : "0.5",
		"descontoCupom" : {
			"444" : "50"
		}
	}
	$scope.produtos = [
	{
		id: "0",
		nome: "Bike 1",
		referencia: "5566779922",
		quantidade: "1",
		PrecoItau: "750",
		PrecoNormal: "1500",
		img: "static/img/thumb-bike.png"
	},
	{
		id: "1",
		nome: "Bike 2",
		referencia: "5566779922",
		quantidade: "1",
		PrecoItau: "750",
		PrecoNormal: "1500",
		img: "static/img/thumb-bike.png"
	},
	{
		id: "2",
		nome: "Bike 3",
		referencia: "5566779922",
		quantidade: "1",
		PrecoItau: "750",
		PrecoNormal: "1500",
		img: "static/img/thumb-bike.png"
	}
	]
	$scope.TotalItau = 0;
	$scope.TotalNormal = 0;
	$scope.Frete = 0;
	$scope.cep = "0";
	$scope.desconto = 0;
	$scope.totalDescItau = 0;
	$scope.totalDesc = 0;
  // calcular total
  var i = 0;
  $scope.TotalItau = 0;
  $scope.TotalNormal = 0;

  // init Calcular Total e desconto itau
  for (i = 0; i < $scope.produtos.length; i++) {
  	$scope.TotalItau = $scope.TotalItau + $scope.produtos[i].PrecoItau*$scope.produtos[i].quantidade;
  	$scope.TotalNormal = $scope.TotalNormal + $scope.produtos[i].PrecoNormal*$scope.produtos[i].quantidade;

  }
  // init Calcular desconto Itau
  $scope.totalDescItau = ($scope.TotalItau - $scope.TotalItau*$scope.tabelaDescontos.descontoItau) + Number($scope.desconto);

  // calcular Total
  $scope.calcularTotal = function() {
  	$scope.TotalItau = 0;
  	$scope.TotalNormal = 0;

  	for (i = 0; i < $scope.produtos.length; i++) {
  		$scope.TotalItau += $scope.produtos[i].PrecoItau*$scope.produtos[i].quantidade;
  		$scope.TotalNormal += $scope.produtos[i].PrecoNormal*$scope.produtos[i].quantidade;
  	}
  	$scope.totalDescItau = ($scope.TotalItau - $scope.TotalItau*$scope.tabelaDescontos.descontoItau) + Number($scope.desconto);
  	$scope.TotalNormal += Number($scope.Frete) - Number($scope.desconto);
  	$scope.TotalItau += Number($scope.Frete)-Number($scope.desconto);
  }

  // calcular Frete
  $scope.calcularFrete = function(cep) {
  	var resultadoCEP = helpers.buscarCEP(cep).$promise
  	.then(function success(result){
  		if(cep=="09090-440"){
  		$scope.Frete = "500";
  		$scope.calcularTotal();
  	}

  	}).catch(function error(msg) {

  	});

  }

	// Calcular Frete
	$scope.calcularCupom = function() {
		if($scope.tabelaDescontos.descontoCupom[$scope.cupom]){
			$scope.desconto = $scope.tabelaDescontos.descontoCupom[$scope.cupom];
			$scope.calcularTotal();
		} else {
			$scope.desconto = 0;
			$scope.calcularTotal();
		}
	}

  // excluir Produtos
  $scope.excluirProduto = function(id){
  	$scope.produtos.splice(id, 1);
  	$scope.calcularTotal();
  }

  // Spinner
  $scope.spinner = 0 ;
  var MAX_UP = 30;
  var MAX_DOWN = 1 ;
  $scope.spinnerUP = function(id){
  	if($scope.produtos[id].quantidade  != MAX_UP){
  		$scope.produtos[id].quantidade = parseInt($scope.produtos[id].quantidade) + 1 ;

  	}
  	$scope.calcularTotal();
  }
  $scope.spinnerDOWN = function(id){
  	if($scope.produtos[id].quantidade  != MAX_DOWN){
  		$scope.produtos[id].quantidade = parseInt($scope.produtos[id].quantidade) - 1 ;
  	}
  	$scope.calcularTotal();
  }
}]);
