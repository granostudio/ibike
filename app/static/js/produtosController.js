iBikeSite.controller('produtosController', ['$scope', '$http','orderByFilter', function($scope,$http, orderBy) {
  var urlPost = 'https://casainteligente.online/grano/listagemProdutos.php';
  var promise =
  $http({
    method  : 'GET',
    url     : urlPost
  });

  promise.then( function(result) {

    $scope.produtos = result.data;
    console.log($scope.produtos);

    $scope.campo = 'valor';
    $scope.reverse = true;
    $scope.produtos = orderBy($scope.produtos, $scope.campo , $scope.reverse);
    $scope.preloader = true;
    console.log($scope.preloader);

  }).catch ( function(result) {
    $scope.preloader = true;
    $scope.erro = true;
  });

  $scope.sortBy = function(campo,reverse){
     /* $scope.reverse = true;
      $scope.campo = campo ;
      $scope.produtos = orderBy($scope.produtos, $scope.campo , false );*/
      $scope.ordenacao = campo;
      $scope.reverse = reverse;
    };

    $scope.preco_bike = 2000;
    $scope.valorFiltro = '';
    $scope.searchFilters = {};
    $scope.changeFilterTo = function(filtro,valor) {
      if(filtro == 'nome' && $scope.checkbox.nome == false){
        delete  $scope.searchFilters[filtro]
      }else{
        console.log(filtro);
        console.log(valor);
        $scope.produtoFiltro = filtro;
        $scope.valorFiltro = valor;
        if(filtro == undefined){
         $scope.preco_bike = 2000;
         $scope.searchFilters = {};
         $scope.strPreco = '';
       }else{
        $scope.searchFilters[filtro] = valor;
      }
    }
    keys = (Object.keys($scope.searchFilters));
    $scope.strFiltros = '';
    for(i=0;i!=keys.length;i++){
      $scope.strFiltros = $scope.strFiltros+ keys[i]+ ' > ';

    }
  }

  $scope.sidebarAtivado = true;
  $scope.ordenarAtivado = true;
  // animar botão filtrar
  $scope.ativarSidebar = function (){
    if(!$scope.sidebarAtivado ){
      $scope.sidebarAtivado = true;
    } else{
      $scope.sidebarAtivado = false;
    }
  }
  $scope.ativarOrdenar = function (){
    if(!$scope.ordenarAtivado ){
      $scope.ordenarAtivado = true;
    } else{
      $scope.ordenarAtivado = false;
    }
  },

  $scope.filtroSliderPreco = function(){

    console.log($scope.preco_bike);
    if($scope.preco_bike != 2000){
      $scope.strPreco = 'preço > ';
    }else{
      $scope.strPreco = '';
    }
  }




}]);
