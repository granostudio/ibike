iBikeSite.controller('produtoInternaController', ['$scope', '$http','acessorios', function($scope,$http, acessorios) {



 $scope.percentual = function(estrela){
    if($scope.totalizadores == undefined){
        return false;
    }
    var total = $scope.totalizadores['total'];
    console.log("TOTAL DE avaliacoes " + total );
    if($scope.totalizadores[estrela] == undefined){
        var totalEstrela = 0 ;
        var percentual = 1 ;
    }else{
        var totalEstrela = $scope.totalizadores[estrela];
        console.log("Total da estrela escolhida " + estrela + " = " + totalEstrela);
        var percentual = parseFloat((100*totalEstrela)/total);
        console.log("Total percentual dela e de " + percentual);
    }
    var $style = {
     "color" : "#cecece",
     "background-color" : "#d9782d",
     "width": percentual+"%",
     "padding" : "0px",
     'height':'15px'
 }
 return $style
}




$scope.carrgarAvaliacoes = function(){

    var urlPost = 'https://casainteligente.online/grano/listagemAvaliacoes.php';
    var promise =
    $http({
        method  : 'GET',
        url     : urlPost
    });   

    promise.then( function(result) {

        $scope.avaliacoes = result.data;
        var totais = (result.data.length);
        $scope.totalizadores = { total : 0 };

        for(i = 0 ; i != totais ; i++){

            if($scope.totalizadores[result.data[i].estrelas] == null){
                $scope.totalizadores[result.data[i].estrelas] = 1 ;
                $scope.totalizadores['total'] += 1;


            }else{
                $scope.totalizadores[result.data[i].estrelas] += 1 ; 
                $scope.totalizadores['total'] += 1;
            }
        }

        $scope.campo = 'data';
        $scope.reverse = true;
        $scope.produtos = orderBy($scope.avaliacoes, $scope.campo , $scope.reverse);

    }).catch ( function(result) {

    });

    
//Buscando o acessorio de outra tela para manter no mesmo lugar
    var promise = acessorios.buscarAcessorios();
    promise.then( function(result) {
    $scope.acessorios = result.data

 }).catch ( function(result) {
});


}

$scope.carrgarAvaliacoes();

}]);


