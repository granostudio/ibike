'use strict';


iBikeSite.controller('recuperarSenhaController', ['$scope', '$http','helpers', function($scope,$http, helpers) {
  $scope.$ = $;
  $scope.envioSucesso =false;
 var urlAlterarSenha = 'https://casainteligente.online/grano/process.php';
  $scope.regex = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z.\d]{7,}/';
$scope.submeterRecuperarSenha = function(isValid){

    $scope.titulo_modal = "Informações";
    if($scope.formData.email == 'a@uol.com.br'){
      
      $scope.mensagem_modal = "E-mail não encontrado"
      $('#modal').modal('show');
    }else{
     var promise = helpers.submeterFormulario(urlAlterarSenha,$scope.formData);
     promise.then( function(result) {
      $scope.envioSucesso = true;
      $scope.sucesso = true;
      $scope.mensagem_modal = "Enviamos um email para você redefinir a sua senha.!";
      $('#modal').modal('show');
      console.log(result);
    }).catch ( function(result) {
      $scope.mensagem_modal = "E-mail não encontrado"
      $('#modal').modal('show');
      $scope.falha = true;
    });
  }

    

}

  
    

}]);
