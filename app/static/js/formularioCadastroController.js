'use strict';
iBikeSite.controller('formularioCadastroController', ['$scope', '$http','helpers', function($scope,$http, helpers) {
 var urlEnviarCadastro = 'https://casainteligente.online/grano/process.php';


  $scope.validarTipoPassword = function(password) {
    $scope.regex = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z.\d]{7,}/';
    if(password.length < 8){
      $scope.forcaSenha = 0 ;
      return false;
    }

      $scope.forcaSenha = helpers.validarTipoPassword(password);
      console.log( $scope.forcaSenha);
      };



 $scope.submeterCadastrarUsuario = function(isValid){
  $scope.NotSenhasIguais= helpers.validarSenha($scope.formData.senha,$scope.formData.senha1);
  $scope.CPFInvalido = helpers.validarCPF($scope.formData.cpf);
     if(!$scope.NotSenhasIguais){//as senhas são iguais
       if ($scope.forcaSenha >= 3){
         if($scope.CPFInvalido ==false){
           var promise = helpers.submeterFormulario(urlEnviarCadastro,$scope.formData);
           promise.then( function(result) {
             $scope.titulo_modal = "Informações";
             $scope.mensagem_modal = "Cadastro enviado com sucesso"
             $('#modal').modal('show');
             console.log(result);
           }).catch ( function(result) {
                $scope.titulo_modal = "Informações";

             $scope.mensagem_modal = "Falha ao efetuar o cadastro"
             $('#modal').modal('show');
           });
         }else{
              $scope.titulo_modal = "Informações";
             $scope.mensagem_modal = "CPF Inválido";
             $('#modal').modal('show');
         }
      }else{
        $scope.titulo_modal = "Informações";
       $scope.mensagem_modal = "A senha deve ser (Maiúscula, minúsculas e numeros com no no mínimo 8 caracteres)";
       $('#modal').modal('show');
      }

    }
    else{
       $scope.titulo_modal = "Informações";
          $scope.mensagem_modal = "Senhas não são iguais";
          $('#modal').modal('show');
    }
  }
}]);
