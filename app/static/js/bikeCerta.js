'use strict';
iBikeSite
.controller('ctrlBikeCerta', ['$scope', '$http',  '$location', '$anchorScroll',
function ($scope, $http, $location, $anchorScroll) {
    $scope.items = [
    {
      "id": 0,
      "img": "http://placehold.it/32x32",
      "name": "Tito Downtown Disc",
      "tamanho":[
         "1,65m e 1,80m", "1,81m"
      ],
      "p4": 0.43,
      "p5": 0.29,
      "p6": 0.57,
      "p7": 0.14,
      "valorOrdenar": 0, "img": "static/img/bike.jpg"

    },
    {
        "id": 1,
        "img": "http://placehold.it/32x32",
        "name": "Tito Downtown Stepthrough",
        "pequeno": [
          "1,65m", "1,65m e 1,80m"
        ],
        "p4": -0.86,
        "p5": -0.14,
        "p6": 1.00,
        "p7": -0.71,
        "valorOrdenar": 0, "img": "static/img/bike.jpg"
    },
    {
        "id": 1,
        "img": "http://placehold.it/32x32",
        "name": "Caloi Andes",
        "pequeno": [
          "1,65m", "1,65m e 1,80m"
        ],
        "p4": 0.29,
        "p5": -0.43,
        "p6": -0.14,
        "p7": 1.00,
        "valorOrdenar": 0, "img": "static/img/bike.jpg"
    },
    {
        "id": 1,
        "img": "http://placehold.it/32x32",
        "name": "Schwinn Madison ",
        "pequeno": [
          "1,65m", "1,65m e 1,80m"
        ],
        "p4": -1.00,
        "p5": -0.57,
        "p6": 0.86,
        "p7": -0.57,
        "valorOrdenar": 0, "img": "static/img/bike.jpg"
    },
    {
        "id": 1,
        "img": "http://placehold.it/32x32",
        "name": "Vela 1 Low Step Elétrica",
        "pequeno": [
          "1,65m", "1,65m e 1,80m"
        ],
        "p4": -0.57,
        "p5": 0.86,
        "p6": -0.57,
        "p7": -0.86,
        "valorOrdenar": 0, "img": "static/img/bike.jpg"
    },
    {
        "id": 1,
        "img": "http://placehold.it/32x32",
        "name": "Vela S Elétrica",
        "pequeno": [
          "1,65m", "1,65m e 1,80m"
        ],
        "p4": -0.29,
        "p5": -1.00,
        "p6": -0.71,
        "p7": 1.00,
        "valorOrdenar": 0, "img": "static/img/bike.jpg"
    },
    {
        "id": 1,
        "img": "http://placehold.it/32x32",
        "name": "Durban Dobrável Bay Pro",
        "pequeno": [
          "1,65m", "1,65m e 1,80m"
        ],
        "p4": -0.14,
        "p5": 0.71,
        "p6": -1.00,
        "p7": -0.14,
        "valorOrdenar": 0, "img": "static/img/bike.jpg"
    },
    {
        "id": 1,
        "img": "http://placehold.it/32x32",
        "name": "Durban Dobrável Bay Pro ",
        "pequeno": [
          "1,65m", "1,65m e 1,80m"
        ],
        "p4": 0.00,
        "p5": 0.57,
        "p6": -0.86,
        "p7": -0.29,
        "valorOrdenar": 0, "img": "static/img/bike.jpg"
    },
    {
        "id": 1,
        "img": "http://placehold.it/32x32",
        "name": "City Kom Preta",
        "pequeno": [
          "1,65m", "1,65m e 1,80m"
        ],
        "p4": -0.43,
        "p5": 0.43,
        "p6": 0.43,
        "p7": 0.00,
        "valorOrdenar": 0, "img": "static/img/bike.jpg"
    },
    {
        "id": 1,
        "img": "http://placehold.it/32x32",
        "name": "City Kom Verde",
        "pequeno": [
          "1,65m", "1,65m e 1,80m"
        ],
        "p4": -0.71,
        "p5": -0.29,
        "p6": 0.71,
        "p7": -0.43,
        "valorOrdenar": 0, "img": "static/img/bike.jpg"
    },
    {
        "id": 1,
        "img": "http://placehold.it/32x32",
        "name": "Oggi 7.0",
        "pequeno": [
          "1,65m", "1,65m e 1,80m"
        ],
        "p4": 0.86,
        "p5": -0.71,
        "p6": 0.00,
        "p7": 0.86,
        "valorOrdenar": 0, "img": "static/img/bike.jpg"
    },
    {
        "id": 1,
        "img": "http://placehold.it/32x32",
        "name": "Houston Mercury HT Disco ",
        "pequeno": [
          "1,65m", "1,65m e 1,80m"
        ],
        "p4": 1.00,
        "p5": -0.86,
        "p6": 0.14,
        "p7": 0.29,
        "valorOrdenar": 0, "img": "static/img/bike.jpg"
    },
    {
        "id": 1,
        "img": "http://placehold.it/32x32",
        "name": "Houston Discovery A29",
        "pequeno": [
          "1,65m", "1,65m e 1,80m"
        ],
        "p4": 0.57,
        "p5": -1.00,
        "p6": -0.29,
        "p7": 0.71,
        "valorOrdenar": 0, "img": "static/img/bike.jpg"
    },
    {
        "id": 1,
        "img": "http://placehold.it/32x32",
        "name": "Houston Atlantis MAD ",
        "pequeno": [
          "1,65m", "1,65m e 1,80m"
        ],
        "p4": 0.14,
        "p5": 0.14,
        "p6": 0.29,
        "p7": 0.43,
        "valorOrdenar": 0, "img": "static/img/bike.jpg"
    },
    {
        "id": 1,
        "img": "http://placehold.it/32x32",
        "name": "Houston Discovery A27,5 ",
        "pequeno": [
          "1,65m", "1,65m e 1,80m"
        ],
        "p4": 0.71,
        "p5": 0.00,
        "p6": -0.43,
        "p7": 0.57,
        "valorOrdenar": 0, "img": "static/img/bike.jpg"
    }
];

$scope.valorFiltro = '';
$scope.searchFilters = [

];
console.log($scope.searchFilters);
$scope.r4 = 0;
$scope.r5 = 0;
$scope.r6 = 0;
$scope.r7 = 0;

$scope.conversao = function(campoR){
    var taxaConversao = -1;
    if(campoR == 0 ){
            taxaConversao = -1;
         console.log('SAI '+ taxaConversao);
        return -1;
    }
      //console.log('ENTRA '+ campoR);
    switch(parseInt(campoR)){
         case 0:
            taxaConversao = -1;
            break;

            case 1:
            taxaConversao = -1;
            break;
        case 2:
            taxaConversao = -0.5;
            break;
        case 3:
            taxaConversao = 0;
            break;
        case 4:
            taxaConversao = 0.5;
            break;
        case 5:
            taxaConversao = 1 ;
            break;
    }
            return taxaConversao;
}

var i = 0;
// atualizar valores de ordenação
$scope.Gandalf = function(){
  for (i = 0; i < $scope.items.length; i++) {
    $scope.items[i].valorOrdenar = ((0.5 * $scope.conversao($scope.r4) * $scope.items[i].p4) + (0.4 *  $scope.conversao($scope.r5) * $scope.items[i].p5) + (0.05 *  $scope.conversao($scope.r6) * $scope.items[i].p6) + (0.05 *  $scope.conversao($scope.r7) * $scope.items[i].p7));
    if(i < $scope.items.length){
      // console.log($scope.items);
    }
  }
  var myEl = angular.element( document.querySelectorAll( '.item' ) );
  myEl.addClass('load');
}

$scope.Gandalf();

}]);
