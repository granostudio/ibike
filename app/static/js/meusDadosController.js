'use strict';


iBikeSite.controller('meusDadosController', ['$scope', '$http','helpers', function($scope,$http, helpers) {
 var urlAlterarCadastro = 'https://casainteligente.online/grano/process.php';

 $scope.enderecos = [
 {
   titulo: "Casa",
   rua: "Rua Pedro de Alencar",
   numero: "234",
   bairro: "Jardim",
   cep: "09692-060",
   cidade: "São Paulo",
   estado: "São Paulo",
   complemento: "apt 2",
   observacoes: ""
 },
 {
   titulo: "Trabalho",
   rua: "Rua Pedro de Alencar",
   numero: "234",
   bairro: "Jardim",
   cep: "09692-060",
   cidade: "São Paulo",
   estado: "São Paulo",
   complemento: "apt 2",
   observacoes: ""
 }]

 // excluir endereço
 $scope.excluirEndereco = function(id){
   $scope.enderecos.splice(id, 1);
 }
 // editar cadsatro
 $scope.boxCadastro = false;
 $scope.editID = 0;
 $scope.novoend= {
   titulo: "Trabalho",
   rua: "Rua Pedro de Alencar",
   numero: "234",
   bairro: "Jardim",
   cep: "09692-060",
   cidade: "São Paulo",
   estado: "São Paulo",
   complemento: "apt 2",
   observacoes: ""
 };
 $scope.editarEndereco = function(id){
   $scope.boxCadastro = true;
   $scope.editID = id;
   $scope.novoend.titulo = $scope.enderecos[id].titulo;
   $scope.novoend.rua = $scope.enderecos[id].rua;
   $scope.novoend.numero = $scope.enderecos[id].numero;
   $scope.novoend.bairro = $scope.enderecos[id].bairro;
   $scope.novoend.CEP = $scope.enderecos[id].cep;
   $scope.novoend.cidade = $scope.enderecos[id].cidade;
   $scope.novoend.estado = $scope.enderecos[id].estado;
   $scope.novoend.complemento = $scope.enderecos[id].complemento;
   $scope.novoend.observacoes = $scope.enderecos[id].observacoes;
 }
 $scope.salvarEndereco = function(){
   $scope.enderecos[$scope.editID].titulo = $scope.novoend.titulo;
   $scope.enderecos[$scope.editID].rua = $scope.novoend.rua;
   $scope.enderecos[$scope.editID].numero = $scope.novoend.numero ;
   $scope.enderecos[$scope.editID].bairro = $scope.novoend.bairro;
   $scope.enderecos[$scope.editID].cep = $scope.novoend.CEP;
   $scope.enderecos[$scope.editID].cidade = $scope.novoend.cidade;
   $scope.enderecos[$scope.editID].estado = $scope.novoend.estado;
   $scope.enderecos[$scope.editID].complemento = $scope.novoend.complemento;
   $scope.enderecos[$scope.editID].observacoes = $scope.novoend.observacoes;

  

   if($scope.novoend.CEP == '' || ($scope.novoend.numero == '' || $scope.novoend.numero == undefined) || $scope.novoend.titulo == undefined){

   }else{
     $scope.boxCadastro = false;
   }
 }

 $scope.submeterAlterarUsuario = function(isValid){
  if(isValid == false){
      $scope.mensagem_modal = "Existem campos que não foram preenchidos corretamente"
         $('#modal').modal('show');
         return false;
  }
  $scope.titulo_modal = "Informações";
  $scope.NotSenhasIguais= helpers.validarSenha($scope.formData.senha,$scope.formData.senha1);
  $scope.CPFInvalido = helpers.validarCPF($scope.formData.cpf);
     if(!$scope.NotSenhasIguais){//as senhas são iguais
      if($scope.CPFInvalido ==false){
        var promise = helpers.submeterFormulario(urlAlterarCadastro,$scope.formData);
        promise.then( function(result) {
          //alert('Cadastro alterado com sucess');

          $scope.titulo_modal = "Informações";

          $scope.mensagem_modal = "Cadastro alterado com sucesso"
          $('#modal').modal('show');

          console.log(result);
        }).catch ( function(result) {
         $

         $scope.mensagem_modal = "Erro ao alterar o cadastro"
         $('#modal').modal('show');
       });
      }else{
        $scope.mensagem_modal = "CPF Inválido"
         $('#modal').modal('show');
      }
    }
  },



  $scope.checarCEP = function(cep){
    if ( cep.length >=9){
      var resultadoCEP = helpers.buscarCEP(cep).$promise
      .then(function success(result){


       $scope.novoend.rua = result.logradouro
       $scope.novoend.numero = '';
       $scope.novoend.bairro = result.bairro
       $scope.novoend.CEP = cep
       $scope.novoend.cidade = result.localidade
       $scope.novoend.estado = result.uf
       $scope.novoend.complemento = '';
       $scope.novoend.observacoes = '';

       numero_input = document.getElementById('numero_input');
       numero_input.focus();

     }).catch(function error(msg) {

     });
   }
 }
}]);
