'use strict';
var urlAlterarSenha = 'https://casainteligente.online/grano/process.php';

iBikeSite.factory('helpers', function($http,$resource) {
  return {
    validarSenha: function(senha1,senha2) {
     var senha1 = new String(senha1).valueOf();
     var senha2 = new String(senha2).valueOf();
     if(senha1 == senha2){
      return false;
    }else{
      return true;
    }
  },

  submeterFormulario: function(urlPost,formData) {
    console.log($resource);
    return (
     $http({
      method  : 'POST',
      url     : urlPost,
              data    : $.param(formData),  // pass in data as strings
              headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
            })
     );
  },


  buscarCEP: function (cep){
    var resultadoCEP = '';
    var rest = $resource(
      'https://viacep.com.br/ws/:cep/json/',
      {
        'cep': ''
      }
      ); 

    return rest.get({'cep': cep})

  },


 
  validarTipoPassword : function(password) {
  var tests = [/[0-9]/, /[a-z]/, /[A-Z]/];
    if(password.length < 8){
      return false;
    }
    var s = 0 ;
    for(var i in tests)
    {
      if(tests[i].test(password))
      {
        s++;
      }
    }
     return s;
    },


    validarCPF : function(cpf){
      var numeros, digitos, soma, i, resultado, digitos_iguais;
      digitos_iguais = 1;
      cpf = cpf.replace(/(\.|\/|\-)/g,"");
      if (cpf.length < 11){
        return true;
      }
      for (i = 0; i < cpf.length - 1; i++)
        if (cpf.charAt(i) != cpf.charAt(i + 1))
        {
          digitos_iguais = 0;
          break;
        }
        if (!digitos_iguais)
        {
          numeros = cpf.substring(0,9);
          digitos = cpf.substring(9);
          soma = 0;
          for (i = 10; i > 1; i--)
            soma += numeros.charAt(10 - i) * i;
          resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
          if (resultado != digitos.charAt(0)){

            return true;
          }
          numeros = cpf.substring(0,10);
          soma = 0;
          for (i = 11; i > 1; i--)
            soma += numeros.charAt(11 - i) * i;
          resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
          if (resultado != digitos.charAt(1)){
            return true;

          }
          return false;
        }
        else{
          return true;
        }

      }  


    };
  }).config(function($mdDateLocaleProvider) {

    // Example of a French localization.
    $mdDateLocaleProvider.months = ['Janeiro', 'Fevereiro', 'Março', 'Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro' ];
    $mdDateLocaleProvider.shortMonths = ['Jan', 'Fev', 'Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez' ];
    $mdDateLocaleProvider.days = ['dimanche', 'lundi', 'mardi' ];
    $mdDateLocaleProvider.shortDays = ['Dom', 'Seg', 'Ter' , 'Qua', 'Qui','Sex','Sab'];

    // Can change week display to start on Monday.
    $mdDateLocaleProvider.firstDayOfWeek = 1;


  });;



