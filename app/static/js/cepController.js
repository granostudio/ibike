iBikeSite.controller('cepCtrl', function($scope, myResource) { 

          $scope.checarCEP = function(){
            if($scope.formData.cep != undefined){
            var quantidadeCep = ($scope.formData.cep.length);
            if(quantidadeCep == 9){
              $scope.findCep();
            }
          }

          }  ,
          //$scope.logradouro = 'teste';
          $scope.findCep = function () {
            if($scope.formData.cep == ''){
              return false;
            }
            myResource.get({'cep': $scope.formData.cep}).$promise
            .then(function success(result){
              console.log(result)
              if(result.erro){
                cep_input = document.getElementById('cep_input');
                cep_input.focus();
                 $scope.formData.endereco = ''
              $scope.formData.bairro_end = ''
              $scope.formData.ciade_end = ''
              $scope.formData.estado  = '';
              $scope.formData.cep = '';
            
              }else{
              $scope.formData.endereco = result.logradouro;
              $scope.formData.bairro_end = result.bairro;
              $scope.formData.ciade_end = result.localidade;
              $scope.formData.estado  = result.uf;
              numero_input = document.getElementById('numero_input');
              numero_input.focus();
            }
              
            }).catch(function error(msg) {
              console.error('Error');
            }); 
          }
        }).factory('myResource', function ($resource) {
          var rest = $resource(
            'https://viacep.com.br/ws/:cep/json/',
            {
              'cep': ''
            }
            ); 
          return rest;
        });