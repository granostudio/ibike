iBikeSite.provider('creditCardInput', function() {
  var _amex, _discover, _master, _visa;
  _amex = 'amex';
  _visa = 'visa';
  _master = 'master';
  _discover = 'discover';
  _elo = 'elo';
  _jcb = 'jcb';
  _hipercard = 'hipercard';
  _diners = 'diners';
  this.setCardClasses = function(cardClassObj) {
    _amex = cardClassObj.americanExpress || 'amex';
    _visa = cardClassObj.visa || 'visa';
    _master = cardClassObj.masterCard || 'master';
    _elo = cardClassObj.elo || 'elo';
    _jcb = cardClassObj.jcb || 'jcb';
    _hipercard = cardClassObj.hipercard || 'hipercard';
    _diners = cardClassObj.diners || 'diners';
    return _discover = cardClassObj.discoverCard || 'discover';
  };
  this.$get = function() {
    return {
      americanExpressClass: _amex,
      visaClass: _visa,
      masterCardClass: _master,
      discoverCardClass: _discover,
      cardClasses: [_amex, _visa, _master, _discover].join(' ')
    };
  };
  return this;
}).directive('type', [
  'creditCardInput', function(creditCardInput) {

    return {
      require: '?ngModel',
      scope: {
            band: '=band'
        },
      link: function(scope, el, attrs, ngModel) {
        var amexFormat, cvcParse, dataParse, easeDelete, formField, format, inputType, parse, standardFormat, validity;
        inputType = attrs.ngType || attrs.type;
        scope.$parent.newUser = [];
        if (!ngModel) {
          return;
        }
        if (!(inputType === 'credit card' || inputType === 'cvc' || inputType === 'data')) {
          return;
        }
        if (inputType === 'cvc') {
          el.on('blur keyup change', function(e) {
            return scope.$apply(function() {
              var text;
              if (!(text = el.val())) {
                return;
              }
              ngModel.$setViewValue(text);
              return el.val(cvcParse(ngModel.$viewValue));
            });
          });
          cvcParse = function(val) {
            var value;
            value = val != null ? val.replace(/([^\d])*/g, '').slice(0, 4) : void 0;
            ngModel.$setValidity('minlength', value.length >= 4 || ngModel.$isEmpty(value));
            return value;
          };
          return ngModel.$parsers.push(cvcParse);
        }
        if (inputType === 'data') {

          el.on('blur keyup change', function(e) {
            return scope.$apply(function() {
              var text;
              if (!(text = el.val())) {
                return;
              }
              ngModel.$setViewValue(text);
              return el.val(dataParse(ngModel.$viewValue));
            });
          });
          dataParse = function(val) {
            var value;
            value = val != null ? val.replace(/([^\d])*/g, '').slice(0, 6) : void 0;
            ngModel.$setValidity('minlength', value.length >= 6 || ngModel.$isEmpty(value));
            value = value.slice(0, 2) + "/" + value.slice(2, 4);
            return value;
          };
          return ngModel.$parsers.push(dataParse);

        } else {
          formField = el.parent();

          el.on('blur keyup change', function(e) {
            return scope.$apply(function() {
              var text;
              if (!(text = el.val())) {
                return;
              }
              ngModel.$setViewValue(text);
              return el.val(format(ngModel.$viewValue));
            });
          });
          parse = function(val) {
            var ref, ref1;

            validity(val);
            if (formField.hasClass(creditCardInput.americanExpressClass)) {
              return (ref = val.replace(/([^\d])*/g, '').slice(0, 15)) != null ? ref : '';
            } else {
              return (ref1 = val.replace(/([^\d])*/g, '').slice(0, 16)) != null ? ref1 : '';
            }
          };
          ngModel.$parsers.push(parse);
          format = function(text) {
            var num, regAmex, regDisc, regMast, regVisa, regElo, regJcb, regHip, regDin;
            if (!text) {
              ngModel.$setPristine();
              return;
            }
            num = text.replace(/([^\d\s])*/g, '');
            regAmex = new RegExp("^(34|37)");
            regVisa = new RegExp("^4");
            regMast = new RegExp("^5[1-5]");
            regDisc = new RegExp("^(301|305|36|38)");
          	regElo  = new RegExp("^(636368|636369|438935|504175|451416|636297|5067|4576|4011|506699)");
          	regJcb  = new RegExp("^(3528|3589)");
            regHip  = new RegExp("^(38|60)");
            regDin  = new RegExp("^(6011|622|64|65)");
            switch (false) {
              case !regAmex.test(num):
                  scope.$parent.newUser.band = {value: creditCardInput.americanExpressClass};
                  break;
              case !regVisa.test(num):
                  scope.$parent.newUser.band = {value: creditCardInput.visaClass};
                  break;
              case !regMast.test(num):
                  scope.$parent.newUser.band = {value: creditCardInput.masterCardClass};
                  break;
              case !regDisc.test(num):
                  scope.$parent.newUser.band = {value: creditCardInput.discoverCardClass};
                  break;
            }
            if (num.length < 2) {
              formField.removeClass(creditCardInput.cardClasses);
            }
            if (num.length === 2) {

              formField.addClass((function() {
                switch (false) {
                  case !regAmex.test(num):
                    return creditCardInput.americanExpressClass;
                  case !regVisa.test(num):
                    return creditCardInput.visaClass;
                  case !regMast.test(num):
                    return creditCardInput.masterCardClass;
                  case !regDisc.test(num):
                    return creditCardInput.discoverCardClass;
                }
              })());
            }
            if (regAmex.test(num)) {
              return amexFormat(num);
            } else {
              return standardFormat(num);
            }
          };
          standardFormat = function(num) {
            if (num[14] === ' ') {
              if (num.length > 18) {
                return num.slice(0, 19);
              }
            }
            if ((num.length === 5 || num.length === 10 || num.length === 15) && num[num.length - 1] !== ' ') {
              return num.slice(0, -1) + ' ' + num[num.length - 1];
            } else if ((num.length === 6 || num.length === 11 || num.length === 16) && num[num.length - 2] !== ' ') {
              return num.slice(0, -2) + ' ' + num.slice(num.length - 2);
            } else if ((num.length === 7 || num.length === 12 || num.length === 17) && num[num.length - 3] !== ' ') {
              return num.slice(0, -3) + ' ' + num.slice(num.length - 3);
            } else if ((num.length === 8 || num.length === 13 || num.length === 18) && num[num.length - 4] !== ' ') {
              return num.slice(0, -4) + ' ' + num.slice(num.length - 4);
            } else if ((num.length === 9 || num.length === 14 || num.length === 19) && num[num.length - 5] !== ' ') {
              return num.slice(0, -5) + ' ' + num.slice(num.length - 5);
            } else {
              return easeDelete(num);
            }
          };
          amexFormat = function(num) {
            if (num.length > 16) {
              return num.slice(0, 17);
            }
            if ((num.length === 5 || num.length === 12) && num[num.length - 1] !== ' ') {
              return num.slice(0, -1) + ' ' + num[num.length - 1];
            } else if ((num.length === 6 || num.length === 13) && num[num.length - 2] !== ' ') {
              return num.slice(0, -2) + ' ' + num.slice(num.length - 2);
            } else if ((num.length === 7 || num.length === 14) && num[num.length - 3] !== ' ') {
              return num.slice(0, -3) + ' ' + num.slice(num.length - 3);
            } else if ((num.length === 8 || num.length === 15) && num[num.length - 4] !== ' ') {
              return num.slice(0, -4) + ' ' + num.slice(num.length - 4);
            } else if ((num.length === 9 || num.length === 16) && num[num.length - 5] !== ' ') {
              return num.slice(0, -5) + ' ' + num.slice(num.length - 5);
            } else {
              return easeDelete(num);
            }
          };
          easeDelete = function(num) {
            if (num[num.length - 1] === ' ') {
              return num.slice(0, -1);
            } else {
              return num;
            }
          };
          return validity = function(text) {
            var luhnArr, sum;
            luhnArr = [[0, 2, 4, 6, 8, 1, 3, 5, 7, 9], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]];
            sum = 0;
            text.replace(/\D+/g, "").replace(/[\d]/g, function(c, p, o) {
              return sum += luhnArr[(o.length - p) & 1][parseInt(c, 10)];
            });
            return ngModel.$setValidity('mod10', !!(sum % 10 === 0 && sum > 0) || ngModel.$isEmpty(text));
          };
        }
      }
    };
  }
])
.controller('resumoDoPedido', ['creditCardInput','$scope' , 'helpers', function(creditCardInput, $scope, helpers){
  $scope.error =[];
  $scope.error.cond = false;
  $scope.error.valor = "";
  $scope.errorCartao =[];
  $scope.errorCartao.cond = true;
  $scope.errorCartao.valor = "Preencha todos os campos corretamente";
  $scope.termosOK = false;
  $scope.errorTermosOK = false;
  $scope.sucesso = false;
  $scope.produto=[];
  $scope.produto.frete=35;
  $scope.produto.quantidade=1;
  $scope.produto.valor= 1498;
  $scope.produto.descontoItau = 0.2;
  $scope.produto.outrosDescontos = 60;
  $scope.funcCartao = function(){
    if(($scope.newUser.nome=="" || $scope.newUser.nome == undefined) ||
       ($scope.newUser.card=="" || $scope.newUser.card == undefined || $scope.newUser.card.length < 15) ||
       ($scope.newUser.data=="" || $scope.newUser.data == undefined) ||
       ($scope.newUser.code=="" || $scope.newUser.code == undefined || $scope.newUser.card.length < 3)){
         $scope.errorCartao.cond = true;
         $scope.errorCartao.valor = "Preencha todos os campos corretamente";
       } else {
         $scope.errorCartao.cond = false;
       }
  };
  $scope.enviarDados = function(){
    if($scope.termosOK == true){
      $scope.titulo_modal = "";
     $scope.mensagem_modal = "Compra efetuada com sucesso!";
     $('#modal').modal('show');
     $scope.errorTermosOK = false;
    } else {
      $scope.errorTermosOK = true;
    }
  };

   $scope.cards = [
     {name: "Visa", value: "visa"},
     {name: "Master", value: "master"},
     {name: "Amex", value: "amex"},
     {name: "Elo", value: "elo"},
     {name: "Discover", value: "discover"},
     {name: "Jcb", value: "jcb"},
     {name: "Hipercard", value: "hipercard"},
     {name: "Diners", value: "diners"}
   ]
   console.log($scope);
   $scope.cartaoItau = "4444444444444444";
   $scope.enderecos = [
     {
     titulo: "Casa",
     rua: "Rua Pedro de Alencar",
     numero: "234",
     bairro: "Jardim",
     cep: "09692-060",
     cidade: "São Paulo",
     estado: "São Paulo",
     complemento: "apt 2",
     observacoes: ""
   },
   {
     titulo: "Trabalho",
     rua: "Rua Pedro de Alencar",
     numero: "234",
     bairro: "Jardim",
     cep: "09692-060",
     cidade: "São Paulo",
     estado: "São Paulo",
     complemento: "apt 2",
     observacoes: ""
   }]
   $scope.enderecoEscolhido = $scope.enderecos[0];

     $scope.checarCEP = function(cep){
    if ( cep.length >=9){
      var resultadoCEP = helpers.buscarCEP(cep).$promise
      .then(function success(result){
        if(result.erro){

           $scope.novoend.rua =  '';
       $scope.novoend.numero = '';
       $scope.novoend.bairro = '';
       $scope.novoend.CEP =  '';
       $scope.novoend.cidade =  '';
       $scope.novoend.estado =  '';
       $scope.novoend.complemento = '';
   $scope.novoend.observacoes = '';

        numero_input = document.getElementById('cep_input');
              numero_input.focus();


        }else{

           $scope.novoend.rua = result.logradouro
       $scope.novoend.numero = '';
       $scope.novoend.bairro = result.bairro
       $scope.novoend.CEP = cep
       $scope.novoend.cidade = result.localidade
       $scope.novoend.estado = result.uf
       $scope.novoend.complemento = '';
   $scope.novoend.observacoes = '';

        numero_input = document.getElementById('numero_input');
              numero_input.focus();


        }



     }).catch(function error(msg) {

     });
   }
 },
   // Cadastrar novo endereço

   $scope.novoEndereco = function(){
     novo = {
       titulo: $scope.novoend.titulo,
       rua: $scope.novoend.rua,
       numero: $scope.novoend.numero,
       bairro: $scope.novoend.bairro,
       cep: $scope.novoend.CEP,
       cidade: $scope.novoend.cidade,
       estado: $scope.novoend.estado,
       complemento: $scope.novoend.complemento,
       observacoes: $scope.novoend.observacoes
     }

     console.log(novo)
     if($scope.novoend.CEP.length == 9){
       if(($scope.novoend.CEP == '' || $scope.novoend.CEP == undefined) || ($scope.novoend.numero == '' || $scope.novoend.numero == undefined) || $scope.novoend.titulo == undefined){
       }else{
          $scope.enderecos.push(novo);
          $scope.boxCadastro = false;
          $scope.btnCadastro = true;
    }
  }else{
    $scope.error.cond = true;
    $scope.error.valor = "O seu CEP está errado";
  }

   }
   $scope.alterarEndereco = function(e){
     $scope.enderecoEscolhido = e;
     $scope.divEnderecos = false;
   }
}]);
