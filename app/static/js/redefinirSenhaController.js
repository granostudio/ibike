'use strict';


iBikeSite.controller('redefinirSenhaController', ['$scope', '$http','helpers', function($scope,$http, helpers) {
 var urlAlterarSenha = 'https://casainteligente.online/grano/process.php';
  

 $scope.validarTipoPassword = function(password) {
    if(password.length < 8){
      $scope.forcaSenha = 0 ;
      return false;
    }
    
$scope.forcaSenha = helpers.validarTipoPassword(password);
      };

$scope.submeterRedefinirSenha = function(isValid){


  $scope.titulo_modal = "Informações";
       

  $scope.NotSenhasIguais= helpers.validarSenha($scope.formData.senha1,$scope.formData.senha);
   if(!$scope.NotSenhasIguais ){//as senhas são iguais
    if($scope.formData.senha == 'Aa12345678'){
       $scope.mensagem_modal = "Falha ao alterar a senha."
      $('#modal').modal('show');
      return false;
    }

     var promise = helpers.submeterFormulario(urlAlterarSenha,$scope.formData);
     promise.then( function(result) {
      $scope.envioSucesso = true
      $scope.sucesso = true;
      $scope.mensagem_modal = "Sua senha foi alterada com sucesso!"
      $('#modal').modal('show');

      console.log(result);
    }).catch ( function(result) {
      $scope.mensagem_modal = "Falha ao alterar a senha."
      $('#modal').modal('show');
      $scope.falha = true;
    });
  }
}

}]);
