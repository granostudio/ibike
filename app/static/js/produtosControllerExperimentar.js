iBikeSite.controller('produtosControllerExperimentar', ['$scope', '$http','orderByFilter', function($scope,$http, orderBy) {
    var urlPost = 'https://casainteligente.online/grano/listagemProdutos.php';
    var promise =
    $http({
        method  : 'GET',
        url     : urlPost
    });

    promise.then( function(result) {

        $scope.produtos = result.data;

        $scope.campo = 'valor';
        $scope.reverse = true;
        $scope.produtos = orderBy($scope.produtos, $scope.campo , $scope.reverse);

    }).catch ( function(result) {

    });

    $scope.sortBy = function(campo){
            $scope.reverse = true;
      $scope.campo = campo ;
      $scope.produtos = orderBy($scope.produtos, $scope.campo , $scope.reverse );
  };

  $scope.valorFiltro = '';
  $scope.changeFilterTo = function(filtro,valor) {
    $scope.produtoFiltro = filtro;
    $scope.valorFiltro = valor;
    }

    $scope.filtrarProduto = function(bikes) {

        var dadoFiltro = $scope.produtoFiltro;
        if(dadoFiltro == undefined){
            return bikes;
        }
        var consultaJson = new String(bikes[dadoFiltro]).valueOf();
        var consultaSearch = new String($scope.valorFiltro).valueOf();
        if(consultaJson != consultaSearch){
            return ;
        }
        return bikes;
    }


    $scope.sidebarAtivado = true;
    $scope.ordenarAtivado = true;
  // animar botão filtrar
  $scope.ativarSidebar = function (){
    if(!$scope.sidebarAtivado ){
        $scope.sidebarAtivado = true;
    } else{
        $scope.sidebarAtivado = false;
    }
}
$scope.ativarOrdenar = function (){
    if(!$scope.ordenarAtivado ){
        $scope.ordenarAtivado = true;
    } else{
        $scope.ordenarAtivado = false;
    }
}


}]);
