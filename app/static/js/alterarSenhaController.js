'use strict';


iBikeSite.controller('alterarSenhaController', ['$scope', '$http','helpers', function($scope,$http, helpers) {
 var urlAlterarSenha = 'https://casainteligente.online/grano/process.php';
  $scope.regex = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z.\d]{7,}/';


   $scope.validarTipoPassword = function(password) {
    if(password.length < 8){
      $scope.forcaSenha = 0 ;
      return false;
    }
    
$scope.forcaSenha = helpers.validarTipoPassword(password);
      };

  
$scope.submeterAlterarSenha = function(isValid){
   $scope.titulo_modal = "Informações";


  $scope.NotSenhasIguais= helpers.validarSenha($scope.formData.senha,$scope.formData.senha1);
   if(!$scope.NotSenhasIguais ){//as senhas são iguais
     var promise = helpers.submeterFormulario(urlAlterarSenha,$scope.formData);
     promise.then( function(result) {
      $scope.envioSucesso = true
      $scope.sucesso = true;

        $scope.mensagem_modal = "Sua senha foi alterada com sucesso!"
      $('#modal').modal('show');


      console.log(result);
    }).catch ( function(result) {
       $scope.mensagem_modal = "Falha ao alterar a senha."
      $('#modal').modal('show');
      $scope.falha = true;
    });
  }
}

}]);
